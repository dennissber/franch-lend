$(document).ready(function () {
	
});


/* modals */
function modalOpen(modal){
  $('.'+modal).slideDown();
  overlayOpen();
}
function modalClose(){
  $(".modalWindowWrap").slideUp();
  overlayClose();
}
$("*[data-modal]").click( function(){
  var modal = $(this).attr("data-modal");
  modalOpen(modal);
});
$(".modalWindowClose").click( function(){
  modalClose();
});
$(".modalCell").click(function(event){
	if ( $(event.target).hasClass('modalCell') ) {
		modalClose();
	}
});

/* overlay */
function overlayOpen(){
  $(".modalOverlay").fadeIn();
  $("html, body").addClass('no-scroll');
}
function overlayClose(){
  $(".modalOverlay").fadeOut();
  $("html, body").removeClass('no-scroll');
}


/* masked input */ 
var mask = "+7 (999) 999-99-99";
var placeholder = {'placeholder':'+7 (___) ___ __ __'};
//var mask = "+38(999) 999-99-99";
//var placeholder = {'placeholder':'+38(___) ___ __ __'};
var user_phone = $('.user_phone_mask');

user_phone.each(function(){
  $(this).mask(mask);
});
user_phone.attr(placeholder);

// if lightSlider 
if (typeof $(this).lightSlider == 'function') {

  var slider_action = $(".slider_action").lightSlider({
    item: 1,
    loop: true,
    pager: false,
    controls: false,
    enableDrag: true,
    adaptiveHeight:true,
    enableTouch: true,
    slideMargin: 0,
    auto: true,
    pause: 5000
  });
  // навигация
  $(".slider__prev").click(function() {
      slider_action.goToPrevSlide();
  }), $(".slider__next").click(function() {
      slider_action.goToNextSlide();
  });

}
// END if lightSlider 